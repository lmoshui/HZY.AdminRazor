﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HZY.DTO.Sys
{
    public class IdsDto<T>
    {

        public List<T> Ids { get; set; }

    }
}
